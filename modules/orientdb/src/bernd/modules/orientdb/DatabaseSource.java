package bernd.modules.orientdb;

import com.orientechnologies.orient.object.db.OObjectDatabaseTx;
import play.inject.BeanSource;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;

public class DatabaseSource implements BeanSource {
    private final ODatabaseDocumentTx documentDB;
    private final OObjectDatabaseTx objectDB;

    public DatabaseSource(int conf) {
        this.documentDB = isEnabled(conf, ODBPlugin.OIV_DOCUMENT_DB) ? ODB.openDocumentDB() : null;
        this.objectDB = isEnabled(conf, ODBPlugin.OIV_OBJECT_DB) ? ODB.openObjectDB() : null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getBeanOfType(Class<T> clazz) {
        if (OObjectDatabaseTx.class.isAssignableFrom(clazz)) {
            return (T) objectDB;
        } else if (ODatabaseDocumentTx.class.isAssignableFrom(clazz)) {
            return (T) documentDB;
        } else {
            return null;
        }
    }

    private boolean isEnabled(int conf, int property) {
        return ((conf & property) == property);
    }

}
