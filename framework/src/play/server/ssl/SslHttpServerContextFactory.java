package play.server.ssl;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;
import java.security.*;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import javax.net.ssl.*;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMDecryptorProvider;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.PasswordFinder;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JceOpenSSLPKCS8DecryptorProviderBuilder;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;
import org.bouncycastle.operator.InputDecryptorProvider;
import org.bouncycastle.pkcs.PKCS8EncryptedPrivateKeyInfo;
import play.Logger;
import play.Play;
import static org.apache.commons.io.IOUtils.closeQuietly;

public class SslHttpServerContextFactory {

    private static final String PROTOCOL = "SSL";
    private static final SSLContext SERVER_CONTEXT;

    static {

        String algorithm = Security.getProperty("ssl.KeyManagerFactory.algorithm");
        if (algorithm == null) {
            algorithm = "SunX509";
        }

        SSLContext serverContext = null;
        KeyStore ks = null;
        try {
            final Properties p = Play.configuration;

            // Made sure play reads the properties
            // Look if we have key and cert files. If we do, we use our own keymanager
            if (Play.getFile(p.getProperty("certificate.key.file", "conf/host.key")).exists()
                && Play.getFile(p.getProperty("certificate.file", "conf/host.cert")).exists())
            {
                Security.addProvider(new BouncyCastleProvider());

                // Initialize the SSLContext to work with our key managers.
                serverContext = SSLContext.getInstance(PROTOCOL);
                TrustManagerFactory tmf = TrustManagerFactory.getInstance(algorithm);
                tmf.init(KeyStore.getInstance(p.getProperty("trustmanager.algorithm", "JKS")));

                serverContext.init(new KeyManager[]{PEMKeyManager.instance}, tmf.getTrustManagers(), null);
            } else {
                // Try to load it from the keystore
                ks = KeyStore.getInstance(p.getProperty("keystore.algorithm", "JKS"));
                // Load the file from the conf
                char[] certificatePassword = p.getProperty("keystore.password", "secret").toCharArray();
                ks.load(new FileInputStream(Play.getFile(p.getProperty("keystore.file", "conf/certificate.jks"))),
                        certificatePassword);

                // Set up key manager factory to use our key store
                KeyManagerFactory kmf = KeyManagerFactory.getInstance(algorithm);
                kmf.init(ks, certificatePassword);
                TrustManagerFactory tmf = TrustManagerFactory.getInstance(algorithm);
                tmf.init(ks);

                // Initialize the SSLContext to work with our key managers.
                serverContext = SSLContext.getInstance(PROTOCOL);
                serverContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            }
        } catch (Exception e) {
            throw new Error("Failed to initialize the server-side SSLContext", e);
        }

        SERVER_CONTEXT = serverContext;
    }

    public static SSLContext getServerContext() {
        return SERVER_CONTEXT;
    }

    public static class PEMKeyManager extends X509ExtendedKeyManager {

        static PEMKeyManager instance = new PEMKeyManager();
        PrivateKey key;
        X509Certificate[] chain;

        public PEMKeyManager() {
            PEMParser keyParser = null;
            PEMParser certParser = null;
            
            try {
                final Properties p = Play.configuration;

                keyParser = new PEMParser(new FileReader(Play.getFile(p.getProperty("certificate.key.file", "conf/host.key"))));
                
                Object object = keyParser.readObject();
                
                JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
                KeyPair kp;
                if (object instanceof PEMEncryptedKeyPair)
                {
                    // Encrypted key - we will use provided password
                    PEMEncryptedKeyPair ckp = (PEMEncryptedKeyPair) object;
                    PEMDecryptorProvider decProv = new JcePEMDecryptorProviderBuilder().build(p.getProperty("certificate.password", "secret").toCharArray());
                    kp = converter.getKeyPair(ckp.decryptKeyPair(decProv));
                    key = kp.getPrivate();
                }
                else if(object instanceof PKCS8EncryptedPrivateKeyInfo) {
                        InputDecryptorProvider pkcs8decoder = new JceOpenSSLPKCS8DecryptorProviderBuilder().build(p.getProperty("certificate.password", "secret").toCharArray());
                        key = converter.getPrivateKey(
                            ((PKCS8EncryptedPrivateKeyInfo)object).decryptPrivateKeyInfo(pkcs8decoder));
                }
                else if(object instanceof RSAPrivateCrtKey){
                    KeyFactory keyFactory=KeyFactory.getInstance("RSA", "BC");
                    RSAPrivateCrtKey pk = (RSAPrivateCrtKey) object;
                    RSAPublicKeySpec publicKeySpec=new RSAPublicKeySpec(pk.getModulus(),pk.getPublicExponent());
                    PublicKey publicKey=keyFactory.generatePublic(publicKeySpec);
                    kp = new KeyPair(publicKey, pk);
                    key = kp.getPrivate();
                }
                else if (object instanceof PEMKeyPair)
                {
                    // Unencrypted key - no password needed
                    PEMKeyPair ukp = (PEMKeyPair) object;
                    kp = converter.getKeyPair(ukp);
                    key = kp.getPrivate();
                }
                
                certParser = new PEMParser(new FileReader(Play.getFile(p.getProperty("certificate.file", "conf/host.cert"))));
                
                JcaX509CertificateConverter certconv = new JcaX509CertificateConverter().setProvider("BC");
                List<X509Certificate> chainList = new ArrayList<X509Certificate>();
                Object certHolder;
                
                while ((certHolder = certParser.readObject()) != null) {
                    if (certHolder instanceof X509CertificateHolder) {
                        chainList.add(certconv.getCertificate((X509CertificateHolder) certHolder));
                    }
		}
                
                chain = chainList.toArray(new X509Certificate[1]);
            } catch (Exception e) {
                e.printStackTrace();
                Logger.error(e, "");
            } finally {
                closeQuietly(keyParser);
                closeQuietly(certParser);
            }
        }

        public String chooseEngineServerAlias(String s, Principal[] principals, SSLEngine sslEngine) {
            return "";
        }

        public String[] getClientAliases(String s, Principal[] principals) {
            return new String[]{""};
        }

        public String chooseClientAlias(String[] strings, Principal[] principals, Socket socket) {
            return "";
        }

        public String[] getServerAliases(String s, Principal[] principals) {
            return new String[]{""};
        }

        public String chooseServerAlias(String s, Principal[] principals, Socket socket) {
            return "";
        }

        public X509Certificate[] getCertificateChain(String s) {
            return chain;
        }

        public PrivateKey getPrivateKey(String s) {
            return key;
        }
    }

}
